# SHACL Sample Adaptor

This is an experiment with [shaclex](https://github.com/labra/shaclex) library in an OSLC adaptor.

## Setup

Run Jetty using `mvn clean jetty:run-exploded`.

Navigate to the [OSLC Service Provider Catalog](http://localhost:8080/SampleAdaptor/services/catalog/singleton).

Expected output **in the console**:

    y.b.name.SampleAdaptorManager - Result Solution
    :y +:S OK. Evidences: Checked maxCount(2) for predicate(<http://example.org/p>) on node <http://example.org/y>
     Checked minCount(1) for predicate(<http://example.org/p>) on node <http://example.org/y>
    :x +:S OK. Evidences: Checked maxCount(2) for predicate(<http://example.org/p>) on node <http://example.org/x>
     Checked minCount(1) for predicate(<http://example.org/p>) on node <http://example.org/x>


## License

> Copyright (c) 2023 KTH Royal Institute of Technology
> 
> This program and the accompanying materials are made available under the 
> terms of the Eclipse Public License 2.0 which is available at
> http://www.eclipse.org/legal/epl-2.0.
> 
> SPDX-License-Identifier: EPL-2.0

This project uses code from the Eclipse Lyo project dual-licensed under `EPL-1.0 OR BSD-3-Clause`. We chose to license that code under the terms of `EPL-1.0`.

> Copyright (c) 2012 IBM Corporation and Contributors to the Eclipse Foundation.
> 
>  All rights reserved. This program and the accompanying materials
>  are made available under the terms of the Eclipse Public License v1.0
>  and Eclipse Distribution License v. 1.0 which accompanies this distribution.
>  
>  The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
>  and the Eclipse Distribution License is available at
>  http://www.eclipse.org/org/documents/edl-v10.php.
> 
> SPDX-License-Identifier: EPL-1.0 OR BSD-3-Clause